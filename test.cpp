# include "bass.h"
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <pthread.h>
# include <unistd.h>
# include <signal.h>
extern "C"
{
	# include "mlx.h"
}

void *mlx_ptr;
void *win_ptr;
void *image;
int ln_size;
pthread_t sample_thread;

pthread_mutex_t spectrum_mutex;
unsigned char	spectrum[16];

HSTREAM main_stream;

static void	put_pixel(char *image, int x, int y, int col)
{
		int offset;

		offset = x * 4;
		offset += y * ln_size;
		*(int *)(image + offset) = col;
}

void *stream_thread(void *scrap)
{
	(void)scrap;
	int lines = 16;

	float	fft[1025];
	int x, y, i, kk;
	char *img;
	while (1)
	{
		pthread_mutex_lock(&spectrum_mutex);
		int b0 = 0;
		BASS_ChannelGetData(main_stream, fft, BASS_DATA_FFT2048);
		for (x = 0; x < lines; x++)
		{
			float peak = 0;
			int b1 = (int)pow(2, x * 10 / (lines - 1));
			if (b1 > 1023) b1 = 1023;
			if (b1 <= b0) b1 = b0 + 1;
			for (; b0 < b1; b0++)
			{
				if (peak < fft[1 + b0])
					peak = fft[1 + b0];
			}
			y = (int)(sqrt(peak) * 3 * 255 - 4);
			if (y > 255)
				y = 255;
			if (y < 0)
				y = 0;
			spectrum[x] = (unsigned char)y;
		}
		pthread_mutex_unlock(&spectrum_mutex);
		usleep(40);
	}
	return (0);
}
int key_hook(int keycode, void *scrap)
{
	(void)scrap;
	//printf("%d \n", (int)keycode);
	if (keycode == 53)
	{
		pthread_cancel(sample_thread);
		exit(0);
	}
	return (0);
}
int loop_hook()
{
	void *image;
	char *im_data;
	int i, j, kk, x, y;
	x = 0;
	pthread_mutex_lock(&spectrum_mutex);
	image = mlx_new_image(mlx_ptr, 1024, 720);
	im_data = mlx_get_data_addr(image, &kk, &ln_size, &kk);
	//put_pixel(im_data, 100, 100, 16777215);
	for (i = 0; i < 16; i++)
	{
		for (j = 0; j < 63; j ++)
			for (y = 0; y < spectrum[i]; y++)
				put_pixel(im_data, x + j, y, 6618980);
		x += 64;
	}
	mlx_clear_window(mlx_ptr, win_ptr);
	mlx_put_image_to_window(mlx_ptr, win_ptr, image, 0, 0);
	mlx_destroy_image(mlx_ptr, image);
	if (BASS_ChannelIsActive(main_stream) == BASS_ACTIVE_STOPPED)
		exit(0);
	pthread_mutex_unlock(&spectrum_mutex);
	return (0);
}

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		BASS_Init(-1, 44100, 0, 0, NULL);
		main_stream = BASS_StreamCreateFile(FALSE, argv[1], 0, 0, 0);
		BASS_ChannelPlay(main_stream, 0);
		pthread_mutex_init(&spectrum_mutex, NULL);
		pthread_create(&sample_thread, NULL, stream_thread, NULL);
		mlx_ptr = mlx_init();
		win_ptr = mlx_new_window(mlx_ptr, 1024, 720, "Sound Test");
		mlx_loop_hook(mlx_ptr, loop_hook, NULL);
		mlx_key_hook(win_ptr, key_hook, NULL);
		mlx_loop(mlx_ptr);
	}
}
