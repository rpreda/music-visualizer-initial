# include "bass.h"
# include <stdio.h>
# include <stdlib.h>
# include <math.h>
# include <pthread.h>
# include <unistd.h>
# include <signal.h>
# include "mlx.h"
# define BARS 256
# define SMOOTH 1
# define HEIGHT 600

void *mlx_ptr;
void *win_ptr;
void *image;
int ln_size;
pthread_t sample_thread;

pthread_mutex_t spectrum_mutex;
unsigned char	spectrum[BARS];
unsigned char	smooth[BARS];
unsigned char	smooth2[BARS];

HSTREAM main_stream;


typedef struct		s_color
{
	char			r;
	char			g;
	char			b;
}					t_color;

t_color		hsvtorgb(unsigned char h, unsigned char s, unsigned char v)
{
	t_color			result;
	unsigned char	region;
	unsigned char	fpart;
	unsigned char	p;
	unsigned char	q;
	unsigned char	t;

	region = h / 43;
	fpart = (h - (region * 43)) * 6;
	p = (v * (255 - s)) >> 8;
	q = (v * (255 - ((s * fpart) >> 8))) >> 8;
	t = (v * (255 - ((s * (255 - fpart)) >> 8))) >> 8;
	if (region == 0)
	{
		result.r = v;
		result.g = t;
		result.b = p;
	}
	if (region == 1)
	{
		result.r = q;
		result.g = v;
		result.b = p;
	}
	if (region == 2)
	{
		result.r = p;
		result.g = v;
		result.b = t;
	}
	if (region == 3)
	{
		result.r = p;
		result.g = q;
		result.b = v;
	}
	if (region == 4)
	{
		result.r = v;
		result.g = p;
		result.b = q;
	}
	return (result);
}

static void	put_pixel(char *image, int x, int y, t_color col)
{
	int offset;

	offset = x * 4;
	offset += y * ln_size;
	*(image + offset) = col.r;
	*(image + offset + 1) = col.g;
	*(image + offset + 2) = col.b;
}

void *stream_thread(void *scrap)
{
	(void)scrap;
	int lines = BARS;

	float	fft[1025];
	int x, y, i, kk;
	char *img;
	while (1)
	{
		pthread_mutex_lock(&spectrum_mutex);
		int b0 = 0;
		BASS_ChannelGetData(main_stream, fft, BASS_DATA_FFT2048);
		for (x = 0; x < lines; x++)
		{
			float peak = 0;
			int b1 = (int)pow(2, x * 10 / (lines - 1));
			if (b1 > 1023) b1 = 1023;
			if (b1 <= b0) b1 = b0 + 1;
			for (; b0 < b1; b0++)
			{
				if (peak < fft[1 + b0])
					peak = fft[1 + b0];
			}
			y = (int)(sqrt(peak) * 3 * 255 - 4);
			if (y > 255)
				y = 255;
			if (y < 0)
				y = 0;
			spectrum[x] = (unsigned char)y;
		}
		usleep(7400);
		pthread_mutex_unlock(&spectrum_mutex);
	}
	return (0);
}
int key_hook(int keycode, void *scrap)
{
	(void)scrap;
	if (keycode == 53)
	{
		pthread_cancel(sample_thread);
		exit(0);
	}
	return (0);
}
unsigned int tcol_int(t_color col)
{
	unsigned int ret = 0;

	ret += col.r;
	ret = ret << 8;
	ret += col.g;
	ret = ret << 8;
	ret += col.b;
	return (ret);
}
int loop_hook()
{
	void *image;
	char *im_data;
	int i, j, kk, x, y, aux;;
	x = 0;
	pthread_mutex_lock(&spectrum_mutex);
	if (SMOOTH)
		for (i = 0; i < BARS; i++)
		{
			spectrum[i] = (spectrum[i] + smooth[i]) / 2;
			spectrum[i] = (spectrum[i] + smooth2[i]) / 2;
			aux = spectrum[i];
			if (abs(spectrum[i] - smooth2[i] < 30))
				spectrum[i] = smooth2[i];
			smooth2[i] = smooth[i];
			smooth[i] = aux;
		}
	image = mlx_new_image(mlx_ptr, 1024, HEIGHT);
	im_data = mlx_get_data_addr(image, &kk, &ln_size, &kk);
	for (i = 0; i < BARS; i++)
	{
		int val = spectrum[i] * HEIGHT / 255;
		t_color col;

		col = hsvtorgb(spectrum[i] / 1.5, 255, 255);
		for (j = 0; j < (1024 / BARS) - 1; j ++)
			for (y = HEIGHT - 1; y > HEIGHT - val; y--)
				put_pixel(im_data, x + j, y, col);
		x += (1024 / BARS);
	}
	/*for (i = 0; i < BARS; i++)
		printf("%d ", spectrum[i]);
	printf("\n");*/
	mlx_clear_window(mlx_ptr, win_ptr);
	mlx_put_image_to_window(mlx_ptr, win_ptr, image, 0, 0);
	mlx_destroy_image(mlx_ptr, image);
	if (BASS_ChannelIsActive(main_stream) == BASS_ACTIVE_STOPPED)
		exit(0);
	pthread_mutex_unlock(&spectrum_mutex);
	return (0);
}

int main(int argc, char **argv)
{
	if (argc > 1)
	{
		BASS_Init(-1, 44100, 0, 0, NULL);
		main_stream = BASS_StreamCreateFile(FALSE, argv[1], 0, 0, 0);
		BASS_ChannelPlay(main_stream, 0);
		pthread_mutex_init(&spectrum_mutex, NULL);
		pthread_create(&sample_thread, NULL, stream_thread, NULL);
		mlx_ptr = mlx_init();
		win_ptr = mlx_new_window(mlx_ptr, 1024, HEIGHT, "Sound Test");
		mlx_loop_hook(mlx_ptr, loop_hook, NULL);
		mlx_key_hook(win_ptr, key_hook, NULL);
		mlx_loop(mlx_ptr);
	}
}
